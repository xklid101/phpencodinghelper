#!/bin/bash
#

# set -e

echo "";
# define printing functions
print_danger() {
    COLOR='\033[41m'
    NC='\033[0m' # No Color
    printf "${COLOR}${1}${NC}\n"
}
print_warning() {
    COLOR='\033[1;33m'
    NC='\033[0m' # No Color
    printf "${COLOR}${1}${NC}\n"
}
print_info() {
    COLOR='\033[1;32m'
    NC='\033[0m' # No Color
    printf "${COLOR}${1}${NC}\n"
}
print_success() {
    COLOR='\033[42m'
    NC='\033[0m' # No Color
    printf "${COLOR}${1}${NC}\n"
}

# test for config file parameters
if [ $# -eq 0 ]
then
    print_danger "No config file path provided!"
    echo "Use like \"./encode.sh /some/path/to/proper/configfile_withvariables.conf\""
    exit 1;
fi
if [ ! -f "$1" ]; then
    print_danger "Config file $1 not found!"
    exit 1;
fi

# include first argument as an config file
. "$1"

#check variables needed for script
err=0
if [ -z ${ENCODER_BIN+x} ]
then
    # ENCODER_BIN=/home/xklid101/Downloads/sourceguardian/sourceguardian-evaluation-CLI/bin/sourceguardian
    print_danger "Missing variable \"ENCODER_BIN\"! (phpencoder binary path)"
    err=1
fi
if [ -z ${DIR_FROM+x} ]
then
    # DIR_FROM=~/srv/www/test.anything
    print_danger "Missing variable \"DIR_FROM\"! (base directory to copy files from)"
    err=1
fi
if [ -z ${FILELIST_FROM+x} ]
then
    # FILELIST_FROM=~/srv/phpencoded/encode.test.anything.filelist.txt
    print_danger "Missing variable \"FILELIST_FROM\"! (file path to file with files list with relative paths to \$DIR_FROM to copy)"
    err=1
fi
if [ -z ${DIR_TO+x} ]
then
    # DIR_TO=~/srv/phpencoded/test.anything
    print_danger "Missing variable \"DIR_TO\"! (destination directory)"
    err=1
fi
if [ $err -eq 1 ]
then
    echo "";
    exit 1;
fi

if [ -z ${ITEMS_TO_ENCODE+x} ]
then
    # ITEMS_TO_ENCODE=(
    #   "wwwdata/a/a.php"
    #   "wwwdata/nonexistsnt"
    #   "wwwdata/a/ab"
    # )
    echo ""
    print_warning "Missing variable \"ITEMS_TO_ENCODE\"! (directories and/or files paths array - paths are relative to \$DIR_FROM - to be encoded)!"
    echo "So nothing is going to be encoded! (Every file will be copied as is)"
    echo "Are you sure you want to do this? (y/n)"
    while true; do
        read -p "" yn
        case $yn in
            [Yy] ) echo "Ok, going on..."; break;;
            [Nn] ) exit;;
            * ) echo "Please answer \"y\" for yes or \"n\" for no.";;
        esac
    done
fi

# remove existing destination dir if exists
if [ -d "$DIR_TO" ]
then
    echo ""
    print_warning "DIRECTORY $DIR_TO IS GOING TO BE COMPLETELY REMOVED!"
    echo "Are you sure you want to do this? (y/n)"
    while true; do
        read -p "" yn
        case $yn in
            [Yy] ) echo ""; rm -rvf "$DIR_TO"; break;;
            [Nn] ) exit;;
            * ) echo "Please answer \"y\" for yes or \"n\" for no.";;
        esac
    done
fi

commanderror=0
# create empty destination dir
echo ""
echo ""
mkdir -vp "$DIR_TO"
lastcommanderror=$?
if [ $commanderror -eq 0 ]; then commanderror=$lastcommanderror; fi;
if [ $commanderror -ne 0 ]; then
    echo ""
    print_danger "!!! ERROR (num $commanderror) !!!"
    echo ""
    exit $commanderror;
fi;

# copy files from list into destination
echo ""
echo ""
print_info "FILES FROM $DIR_FROM (ACCORDING TO FILELIST $FILELIST_FROM) IS GOING TO BE COPIED INTO $DIR_TO"
rsync -avR --files-from="$FILELIST_FROM" "$DIR_FROM" "$DIR_TO"
lastcommanderror=$?
if [ $commanderror -eq 0 ]; then commanderror=$lastcommanderror; fi;
if [ $commanderror -ne 0 ]; then
    echo ""
    print_danger "!!! ERROR (num $commanderror) !!!"
    echo ""
    exit $commanderror;
fi;

# encode all directories needed to be encoded
echo ""
echo ""
print_info "SOME FILES OR DIRECTORIES IN $DIR_TO IS GOING TO BE ENCODED BY ENCODERs defined in config"
if [ -z ${ITEMS_TO_ENCODE+x} ]; then
    echo "NO \$ITEMS_TO_ENCODE config variable defined or empty! SO NOTHING IS GOING TO BE ENCODED! Skipping action..."
fi
for ITEM in "${ITEMS_TO_ENCODE[@]}"
do
    echo ""
    filepath="$DIR_TO/$ITEM"
    if [ -e "$filepath" ]
    then
        now=$(date +%s%N)

        #
        # -b- is "without backup"
        # it is ok since we copy files before encoding
        # from the original path with rsync above
        #
        $ENCODER_BIN \
            --phpversion 5.6 \
            --phpversion 7 \
            --phpversion 8 \
            "$filepath" \
            -f "*.php" \
            -r \
            -b-


        lastcommanderror=$?
        if [ $commanderror -eq 0 ]; then commanderror=$lastcommanderror; fi;

        # rm -rv "$filepath.bkp.$now"
        # lastcommanderror=$?
        # if [ $commanderror -eq 0 ]; then commanderror=$lastcommanderror; fi;
    else
        echo "NOT FOUND (in filelist): $filepath"
        echo "skipping..."
    fi
done

echo ""
echo ""
echo ""
if [ $commanderror -ne 0 ]
then
    print_danger "!!!!!! SOME ERRORS OCCURED DURING PROCESSING ($commanderror) !!!!!!!"
    print_danger "Check output above!"
    echo ""
    exit $commanderror
else
    print_success "FINISHED OK!"
    print_success "Check $DIR_TO for files..."
    echo ""
    exit 0
fi
